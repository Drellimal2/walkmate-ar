﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


public class TcpServer
{
    private TcpListener listener;
    public DataManager datastore;


    public TcpServer()
    {
        // Choose a port number for your server
        int port = 12234;
        IPAddress ipAddress = IPAddress.Any;

        listener = new TcpListener(ipAddress, port);
        Debug.Log("Listener Created");
        
    }

    public TcpServer(DataManager datastor)
    {
        // Choose a port number for your server
        int port = 12234;
        IPAddress ipAddress = IPAddress.Any;

        listener = new TcpListener(ipAddress, port);
        datastore = datastor;
        Debug.Log("Listener Created");
        
    }

    public void StopServer()
    {
        listener.Stop();
        Console.WriteLine("Server stopped.");
    }

    public async Task StartServerAsync()
    {
        listener.Start();
        Console.WriteLine("Server started. Waiting for connections...");
        Debug.Log("Server started. Waiting for connections...");

        while (true)
        {
            TcpClient client = await listener.AcceptTcpClientAsync();
            _ = HandleClientAsync(client);
        }
    }

    private async Task HandleClientAsync(TcpClient client)
    {
        try
        {
            Console.WriteLine($"Client connected: {client.Client.RemoteEndPoint}");
            Debug.Log($"Client connected: {client.Client.RemoteEndPoint}");

            NetworkStream stream = client.GetStream();
            byte[] buffer = new byte[1024];

            while (true)
            {
                int bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length);
                if (bytesRead == 0)
                {
                    break; // Client disconnected
                }

                string receivedData = Encoding.UTF8.GetString(buffer, 0, bytesRead);

                string[] parts = receivedData.Trim().Split('|');
                
                if (parts.Length > 0)
                {

                    string lg = "log::" + parts[0];
                    datastore.LogData(lg);
                    string switchValue = parts[0];
                    // Use the switch value as needed
                    Debug.Log("Switch Value: " + switchValue);
                    switch (switchValue)
                    {
                        case "1":
                        
                            Debug.Log("Switch case 1");
                            // Code to execute for case 1
                            datastore.logurl = parts[1];
                            break;

                        case "2":
                            Debug.Log("Switch case 2");
                            datastore.log = parts[1] == "1";
                            // Code to execute for case 2
                            break;

                        case "3":
                            lg = "log::" + parts[0] + "::" + parts[1] + "::" + parts[2];
                            datastore.LogData(lg);
                            Debug.Log("Switch case 3");
                            switch(parts[1]){
                                case "p":
                                    if (parts.Length == 4){
                                        datastore.timeToChange =  double.Parse(parts[3]);
                                    }
                                    datastore.changePhase(float.Parse(parts[2]));
                                    break;
                                case "s":
                                    // datastore.setMomoage(parts[2] == "1");
                                    if (parts.Length == 4){
                                        datastore.setMomoage(int.Parse(parts[2]), int.Parse(parts[3]));
                                    } else {
                                        datastore.setMomoage(int.Parse(parts[2]));
                                    }
                                    lg = "log::momoage" + parts[0];
                                    datastore.LogData(lg);
                                    break;
                                case "d":
                                    break;
                            }
                            // Code to execute for case 3
                            break;

                        default:
                            Debug.Log("Default case");
                            // Code to execute if none of the cases match
                            break;
                    }
                }
                else
                {
                    Debug.Log("Invalid input string format");
                }
                Console.WriteLine($"Received from client: {receivedData}");
                Debug.Log($"Received from client: {receivedData}");


                // Process the received data and prepare a response
                string response = "Response from server";

                byte[] responseBytes = Encoding.UTF8.GetBytes(response);
                await stream.WriteAsync(responseBytes, 0, responseBytes.Length);
                await stream.FlushAsync();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
        finally
        {
            client.Close();
            Console.WriteLine($"Client disconnected: {client.Client.RemoteEndPoint}");
        }
    }
}
