﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.Networking;

public class DataManager : MonoBehaviour
{

    public string logurl = "";
    public bool log = false;

    public bool change = false;

    public double timeToChange = 15.0;
    private double elapsed = 0;

    private double percentageChange = 0;

    private const float startingPhase = 0;
    private const float startingDist = 5;

    private float dstchange = 0;
    private float phseChange = 0; 

    public GameObject WalkMateManager;
    public GameObject AvatarManager;
    private WalkMate walkmate;
    private AnimController animcontroller;

    public FileWriter fileWriter;


    // Start is called before the first frame update
    void Start()
    {
        logurl = "http://192.168.8.219:5000/log/";
        log = false;
        walkmate = WalkMateManager.GetComponent<WalkMate>();
        animcontroller = AvatarManager.GetComponent<AnimController>();


    }

    // Update is called once per frame
    void Update()
    {
        if (change) {
            elapsed += Time.deltaTime;
            percentageChange = Math.Min(elapsed, timeToChange) /timeToChange;
            float newphse = startingPhase + ((float)percentageChange * phseChange);
            float newdst = startingDist +  ((float)percentageChange * phseChange);
            setPhaseDiff(newphse);
            
            if (elapsed >= timeToChange) {
                change = false;
            }
        }
    }

    public void setLogUrl(string url) {
        logurl = url;
    }

    public void changePhase(float pd){
        change = true;
        phseChange = pd;
    }

    public void changeDst(float dst){
        change = true;
        dstchange = dst;
    }

    public void setPhaseDiff(float pd){
        walkmate.setTargetPhase(pd);
    }

    public void setMomoage(){
        animcontroller.momoage();
    }

    public void setMomoage(bool mix){
        animcontroller.momoage(mix);
    }

    public void setMomoage(int ind){
        animcontroller.momoage(ind);
    }

    public void setMomoage(int ind, int chngeTime){
        animcontroller.momoage(ind, chngeTime);
    }

    // public void setDistance(float dst){
    //     int a = 2+2;
    // }



    public void LogData(string uri){
        long currentTimeMillis = (long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds;

        // fileWriter.WriteToFile(uri + "::" + currentTimeMillis);
        // Debug.Log("Logging " + uri);
        if(log) {
            Debug.Log("Logging Really");
            // long currentTimeMillis = (long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds;
            StartCoroutine(LogRequest(uri + "::" + currentTimeMillis));
        }
    }

    IEnumerator LogRequest(string uri)
    {
        String path = logurl + uri;
        Debug.Log(path);
        using (UnityWebRequest webRequest = UnityWebRequest.Get(path))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

        }
    }
}
