﻿

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using TMPro;

using UnityEngine;
using UnityEngine.SceneManagement;


#if !UNITY_EDITOR
using System.Threading.Tasks;
#endif

public class ViewerTcpClient : MonoBehaviour
{

    #if !UNITY_EDITOR
    private bool _useUWP = true;
    private Windows.Networking.Sockets.StreamSocket socket;
    private Task exchangeTask;
#endif

#if UNITY_EDITOR
    private bool _useUWP = false;
    System.Net.Sockets.TcpClient client;
    System.Net.Sockets.NetworkStream stream;
    private Thread exchangeThread;
#endif

    private Byte[] bytes = new Byte[256];
    private StreamWriter writer;
    private StreamReader reader;

    private float startingDist = 5f;

    public GameObject avatar, walkMateManager;

    public TextMeshProUGUI textM;

    public CameraLock camLock;
    WalkMate walkMate;
    Vector3 rot;
    double dist =5d;
    double pd = 0;

    AnimController anim;


    public void Connect(string host, string port)
    {
        if (_useUWP)
        {
            ConnectUWP(host, port);
        }
        else
        {
            ConnectUnity(host, port);
        }
    }


    #if UNITY_EDITOR
    private void ConnectUWP(string host, string port)
#else
    private async void ConnectUWP(string host, string port)
#endif
    {
#if UNITY_EDITOR
        errorStatus = "UWP TCP client used in Unity!";
#else
        try
        {
            if (exchangeTask != null) StopExchange();
        
            socket = new Windows.Networking.Sockets.StreamSocket();
            Windows.Networking.HostName serverHost = new Windows.Networking.HostName(host);
            await socket.ConnectAsync(serverHost, port);
        
            Stream streamOut = socket.OutputStream.AsStreamForWrite();
            writer = new StreamWriter(streamOut) { AutoFlush = true };
        
            Stream streamIn = socket.InputStream.AsStreamForRead();
            reader = new StreamReader(streamIn);

            RestartExchange();
            successStatus = "Connected!";
        }
        catch (Exception e)
        {
            errorStatus = e.ToString();
        }
#endif
    }

    private void ConnectUnity(string host, string port)
    {
#if !UNITY_EDITOR
        errorStatus = "Unity TCP client used in UWP!";
#else
        try
        {
            if (exchangeThread != null) StopExchange();

            client = new System.Net.Sockets.TcpClient(host, Int32.Parse(port));
            stream = client.GetStream();
            reader = new StreamReader(stream);
            writer = new StreamWriter(stream) { AutoFlush = true };

            RestartExchange();
            successStatus = "Connected!";
        }
        catch (Exception e)
        {
            errorStatus = e.ToString();
        }
#endif
    }



private bool exchanging = false;
    private bool exchangeStopRequested = false;
    private string lastPacket = null;

    private string errorStatus = null;
    private string warningStatus = null;
    private string successStatus = null;
    private string unknownStatus = null;
    Boolean isfirst  = true;



    public void RestartExchange()
    {
#if UNITY_EDITOR
        if (exchangeThread != null) StopExchange();
        exchangeStopRequested = false;
        exchangeThread = new System.Threading.Thread(ExchangePackets);
        exchangeThread.Start();
#else
        if (exchangeTask != null) StopExchange();
        exchangeStopRequested = false;
        exchangeTask = Task.Run(() => ExchangePackets());
#endif
    }


    // Start is called before the first frame update
    void Start()
    {

        // string fl = "C://Documents/serv.txt";

        // StreamReader readr = new StreamReader(fl);
        // string text = readr.ReadToEnd();
        


        // Connect("192.168.43.204", "5656"); 
        // Change IP address here to be IP address of viewer;
        // Connect("192.168.0.11", "5656");
        Connect("192.168.43.165", "5656");

        
        

        walkMate = walkMateManager.GetComponent<WalkMate>();
        anim = avatar.GetComponent<AnimController>();

        
    }

    public void Update()
    {
        if(lastPacket != null)
        {
            ReportDataToTrackingManager(lastPacket);
            lastPacket = null;
        }

        if(errorStatus != null)
        {
            // StatusTextManager.SetError(errorStatus);
            errorStatus = null;
        }
        if (warningStatus != null)
        {
            // StatusTextManager.SetWarning(warningStatus);
            warningStatus = null;
        }
        if (successStatus != null)
        {
            // StatusTextManager.SetSuccess(successStatus);
            successStatus = null;
        }
        if (unknownStatus != null)
        {
            // StatusTextManager.SetUnknown(unknownStatus);
            unknownStatus = null;
        }

        getDistance();

    }


    private void ReportDataToTrackingManager(string data)
    {
        if (data == null)
        {
            Debug.Log("Received a frame but data was null");
            return;
        }

        var parts = data.Split(';');
        foreach(var part in parts)
        {
            ReportStringToTrackingManager(part);
        }
    }


public void ExchangePackets()
    {
        while (!exchangeStopRequested)
        {
            if (writer == null || reader == null) continue;
            exchanging = true;
            if(isfirst){
                isfirst = !isfirst;
                writer.Write("0:0:0:Holo\n");
            }else {
                string res = getRes();
                writer.Write(res);
            }
            
            Debug.Log("Sent data!");
            string received = null;

#if UNITY_EDITOR
            byte[] bytes = new byte[client.SendBufferSize];
            int recv = 0;
            while (true)
            {
                recv = stream.Read(bytes, 0, client.SendBufferSize);
                received += Encoding.UTF8.GetString(bytes, 0, recv);
                if (received.EndsWith("\n")) break;
            }
#else
            received = reader.ReadLine();
#endif

            lastPacket = received;
            Debug.Log("Read data: " + received);

            exchanging = false;
        }
    }


private void ReportStringToTrackingManager(string inputString)
    {
        var parts = inputString.Split(':');
        Debug.Log(
            "recv: " + inputString);

        
        if(parts.Length > 2){
            double dinc = Double.Parse(parts[0]);
            double pinc = Double.Parse(parts[2]);
            double sinc = Double.Parse(parts[1]);
            if (dinc == -1337 && pinc == -1337 && sinc == -1337){
                // SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                restart();
            } else {

                UpdateDist(dinc);
                UpdatePhase(pinc);
                UpdateStride(sinc);
            }
        }

    }

    private void restart(){
        camLock.init();
        walkMate.Init();
        anim.init();
    }

    private void ReportStringToTrackingManager(string inputString, string old)
    {
        var parts = inputString.Split(':');
        Debug.Log(
            "recv: " + inputString);

        // var positionData = parts[1].Split(',');
        // var rotationData = parts[2].Split(',');

        double dinc = Double.Parse(parts[0]);
        double pinc = Double.Parse(parts[1]);

        UpdateDist(dinc);
        UpdatePhase(pinc);



        // float x = float.Parse(positionData[0]);
        // float y = float.Parse(positionData[1]);
        // float z = float.Parse(positionData[2]);

        
        // float qx = float.Parse(rotationData[0]);
        // float qy = float.Parse(rotationData[1]);
        // float qz = float.Parse(rotationData[2]);
        // float qw = float.Parse(rotationData[3]);

        // Vector3 position = new Vector3(x, y, z);
        // Quaternion rotation = new Quaternion(qx, qy, qz, qw);

        // TrackingManager.UpdateRigidBodyData(id, position, rotation);
    }

    public void StopExchange()
    {
        exchangeStopRequested = true;

#if UNITY_EDITOR
        if (exchangeThread != null)
        {
            exchangeThread.Abort();
            stream.Close();
            client.Close();
            writer.Close();
            reader.Close();

            stream = null;
            exchangeThread = null;
        }
#else
        if (exchangeTask != null) {
            exchangeTask.Wait();
            socket.Dispose();
            writer.Dispose();
            reader.Dispose();

            socket = null;
            exchangeTask = null;
        }
#endif
        writer = null;
        reader = null;
    }

    public void OnDestroy()
    {
        StopExchange();
    }



    public void UpdateDist(double mult){
        rot = avatar.transform.rotation.eulerAngles;
        double ang = rot.y * Math.PI/180;
        float xinc =  (startingDist + (float)mult) * (float)Math.Sin(ang);

        float zinc =  (startingDist + (float)mult) *(float) Math.Cos(ang);
        Vector3 pos = avatar.transform.position; 
        Vector3 campos = Camera.main.transform.position; // #
        // avatar.transform.position = new Vector3(xinc, pos.y, zinc); // Vector3.Lerp(pos, newPos, Time.deltaTime * 2.5f); #
        // avatar.transform.position = new Vector3(campos.x + xinc, pos.y, campos.z + zinc); // #
        camLock.distance = startingDist + (float)mult;
        anim.switchAnim();
    }


    public void UpdateStride(int val){
        switch(val){
            case 1:
                anim.switchStride(1);
                break;
            case -1:
                anim.switchStride(-1);
                break;
            default:
                break;
        }
    }

    public void UpdateStride(double val){
        Debug.Log(val);
        anim.updateStride((float)val);
    }

    public void UpdatePhase(int mult){

        float phase = mult * 20.0f * Mathf.Deg2Rad;
        walkMate.setTargetPhase(phase);


    }

    public void UpdatePhase(double mult){

        float phase = (float) mult * 30.0f * Mathf.Deg2Rad;
        walkMate.setTargetPhase(phase);


    }


    public double getDist(Vector3 pos){
        Vector3 campos = Camera.main.transform.position;

        // dist = Math.Sqrt(Math.Pow(pos.x, 2) + Math.Pow(pos.z, 2)); // #
        Double dis = Math.Sqrt(Math.Pow(pos.x - campos.x, 2) + Math.Pow(pos.z - campos.z, 2));
        // Debug.Log("Distance is " + dist);
        
        return dis;
    }

    public double getDistance(){
        Vector3 pos = avatar.transform.position;
        Vector3 campos = Camera.main.transform.position;

        // dist = Math.Sqrt(Math.Pow(pos.x, 2) + Math.Pow(pos.z, 2)); // #
        dist = Math.Sqrt(Math.Pow(pos.x - campos.x, 2) + Math.Pow(pos.z - campos.z, 2));
        // Debug.Log("Distance is " + dist);
        // textM.text = "D : " + dist + "\n" +
        //             "D2: " +   Math.Sqrt(Math.Pow(pos.x, 2) + Math.Pow(pos.z, 2)) + "\n" +
        //             "C : " + campos.ToString() + "\n" +
        //             "CR : " + Camera.main.transform.eulerAngles.ToString() + "\n" + 
        //             "A : " + pos.ToString();

        return dist;

    }

    private string getRes(){
        double phse = walkMate.getTargetPhase()/Mathf.Deg2Rad;
        double strd = anim.stride_inc;
        
        string res = phse + ":" + dist + ":" + strd + "\n";
        return res;
    }


    public void ProcessPacket(string packet){
        var contents = packet.Trim().Split(';');

        int command = Int16.Parse(contents[0]);
        // string params = contents[1];
        switch(command){
            case Commands.REQUEST_SETTINGS:
                Debug.Log("Request");
                break;
            case Commands.UPDATE_SETTINGS:
                Debug.Log("Update");
                break;
            default:
                break;
        }

    }


}
