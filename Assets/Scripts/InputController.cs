﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    public string url = "http://192.168.1.3:5000/experiment/settings";
    private WalkMate walkMate;
    private CameraLock cameraLock;
    private AnimController animcontroller;
    // private DataRecorder dataRecorder;
    // private DebugWindow debugWindow;
    private FootContact footContact;
    public GameObject AvatarManager, WalkMateManager;
    private bool runFlag = false;
    private bool change = false;
    private string mess;
    string state;
    //Sound Play
    AudioSource[] audios;
    // Start is called before the first frame update
    
    
    void Start()
    {
        walkMate = WalkMateManager.GetComponent<WalkMate>();
        audios = WalkMateManager.GetComponents<AudioSource>();
        footContact = WalkMateManager.GetComponent<FootContact>();
        cameraLock = AvatarManager.GetComponent<CameraLock>();
        animcontroller = AvatarManager.GetComponent<AnimController>();

        runFlag = false;
    }

    public void switchAnim(){

    }

    public void start(){
        if (!runFlag){
            footContact.turnOnDetect();
            cameraLock.lockView();
            animcontroller.init();
            // debugWindow.walkState();
            walkMate.turnOnWalkMate();
            // dataRecorder.turnOnRecorder();
            runFlag = true;
        }

    }


    public void shutdown(){
        if (runFlag)
        {
            footContact.turnOffDetect();
            walkMate.turnOffWalkMate();
            cameraLock.freeView();
            // dataRecorder.turnOffRecorder();
            // debugWindow.holdState();
            runFlag = false;
        }
    
    }

    public string getState()
    {
        return state;
    }
    public float getMomoWeight()
    {
        return animcontroller.getWeight();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.S)){
            animcontroller.switchAnim(); // Male Avatar
        }


        if (Input.GetKeyUp(KeyCode.J)){
            // StartCoroutine(getSettings());
        }
    }

    // IEnumerator getSettings(){
    //     // url = "http://192.168.1.3:5000/experiment/settings";
    //     UnityWebRequest request = UnityWebRequest.Get(url);
    //     yield return request.SendWebRequest();

    //     if(request.isNetworkError || request.isHttpError){
    //         Debug.Log(request.error);

    //     }
    //     else {
    //         while(request.downloadProgress < 1){
                
    //         }
    //         string response = request.downloadHandler.text;
    //         ExpSett sett = ExpSett.CreateFromJSON(response);
    //         animcontroller.setAnims(sett.stride);
    //         foreach(int x in sett.stride){
    //             Debug.Log(x);
    //         }


    //     }
    // }
}
