using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Runtime.InteropServices;
using System.IO;
using UnityEngine.Networking;

#if ENABLE_WINMD_SUPPORT
using HL2UnityPlugin;
#endif

public class HoloInfoRecorder2 : MonoBehaviour
{
#if ENABLE_WINMD_SUPPORT
    HL2ResearchMode researchMode;
#endif
    float[] accelSampleData = null;

    float[] gyroSampleData = null;

    float[] magSampleData = null;

    string path;
    string time, timeOut;
    string[] timeArr;
    float posX, posY, posZ;
    float accelX, accelY, accelZ;
    float gyroX, gyroY, gyroZ;
    float magX, magY, magZ;
    //public float timeInterval = 0.1f;
    //private float timeTrigger;
    private float timeElapsed = 0.0f;
    private float Hz = 20.0f;
    void Start()
    {
        string s = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
        path = Application.persistentDataPath + $"\\{s}.csv";
        Debug.Log("ok started");
#if ENABLE_WINMD_SUPPORT
        researchMode = new HL2ResearchMode();
        researchMode.InitializeAccelSensor();
        researchMode.InitializeGyroSensor();
        researchMode.InitializeMagSensor();

        researchMode.StartAccelSensorLoop();
        researchMode.StartGyroSensorLoop();
        researchMode.StartMagSensorLoop();
#endif
        InvokeRepeating("SaveFile", 0.0f, 1.0f/Hz);
        //StartCoroutine(FuncCoroutine());
    }

    bool startRealtimePreview = true;
    
    /*
    IEnumerator FuncCoroutine(){
        while(true){
            SaveFile();

            yield return new WaitForSeconds(timeInterval);
        }
    }
    */
    /*
    void Update(){
        timeElapsed += Time.deltaTime;
        if (timeElapsed >= timeInterval){
            timeElapsed = 0.0f;
            SaveFile();
        }
    }
    */

    public void StopSensorsEvent()
    {
#if ENABLE_WINMD_SUPPORT
        researchMode.StopAllSensorDevice();
#endif
        startRealtimePreview = false;
    }

    private void OnApplicationFocus(bool focus)
    {
        if (!focus) StopSensorsEvent();
    }

    public void SaveFile()
    {
        Debug.Log("Maybe");
        using (StreamWriter sw = new StreamWriter(path, true))
        {
#if ENABLE_WINMD_SUPPORT
            // update Accel Sample
            if (researchMode.AccelSampleUpdated())
            {
                accelSampleData = researchMode.GetAccelSample();
            }
            
            // update Gyro Sample
            if (researchMode.GyroSampleUpdated())
            {
                gyroSampleData = researchMode.GetGyroSample();
            }

            // update Mag Sample
            if (researchMode.MagSampleUpdated())
            {
                magSampleData = researchMode.GetMagSample();
            }
            
#endif
            posX = Camera.main.transform.position.x;
            posY = Camera.main.transform.position.y;
            posZ = Camera.main.transform.position.z;
            if (accelSampleData.Length == 3){
                accelX = accelSampleData[2];
                accelY = accelSampleData[0];
                accelZ = accelSampleData[1];
            }
            
            if (gyroSampleData.Length == 3){
                gyroX = accelSampleData[2];
                gyroY = accelSampleData[0];
                gyroZ = accelSampleData[1];
            }
            if (magSampleData
            .Length == 3){
                magX = accelSampleData[2];
                magY = accelSampleData[0];
                magZ = accelSampleData[1];                
            }
            

            time = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff");
            DateTimeOffset now = DateTimeOffset.UtcNow;
            long unixTimeMilli = now.ToUnixTimeMilliseconds();
            //timeArr = time.Split('-');
            //timeOut = timeArr[4] + ":" + timeArr[5] + ":" + timeArr[6];

            string info = $"rd,{unixTimeMilli},{posX},{posY},{posZ},{accelX},{accelY},{accelZ},{gyroX},{gyroY},{gyroZ},{magX},{magY},{magZ}";
            // string info = $"{unixTimeMilli},{posX},{posY},{posZ},{accelX},{accelY},{accelZ}";
            // MyLogger.Log(info);
            sw.WriteLine(info);
        }
    }


    IEnumerator GetRequest(string data)
    {
        string uri = "http://192.168.0.41:5000/" + data;
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

           
        }
    }
}