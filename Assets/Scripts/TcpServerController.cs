﻿using UnityEngine;
using System.Threading.Tasks;

public class TcpServerController : MonoBehaviour
{
    private TcpServer server;
    public DataManager datastore;

    private async void Start()
    {
        Debug.Log("Server Controller Started" );
        server = new TcpServer(datastore);
        await server.StartServerAsync();
    }

    private void OnDestroy()
    {
        if (server != null)
        {
            server.StopServer();
        }
    }
    
}
