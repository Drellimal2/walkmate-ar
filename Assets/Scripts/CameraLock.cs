﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLock : MonoBehaviour
{

    private bool isRun = false;
    private float y_angle,height;
    public float distance = 5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void init() {
        freeView();
        
        distance = 5f;

    }

    public void updateDistance(float dst){
        distance = dst;
    }


    void locked()
    {
        Vector3 newpos;
        float y = Camera.main.transform.localEulerAngles.y;
        y_angle = y * Mathf.PI / 180;
        newpos.x = Camera.main.transform.position.x + distance * Mathf.Sin(y_angle);
        newpos.y = height;
        newpos.z=  Camera.main.transform.position.z + distance * Mathf.Cos(y_angle);
        transform.position = newpos;
        transform.localEulerAngles = new Vector3(0, y, 0);
    }

    void follow()
    {
        Vector3 newPos;
        float y = Camera.main.transform.localEulerAngles.y;
        y_angle = y * Mathf.PI / 180;
        newPos = new Vector3(
            Camera.main.transform.position.x + distance * Mathf.Sin(y_angle),
            Camera.main.transform.position.y - 1.65f,
            Camera.main.transform.position.z + distance * Mathf.Cos(y_angle)
        );
        height = newPos.y;
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * 2.5f);
        transform.localEulerAngles = new Vector3(0, y, 0);
    }

    public void lockView()
    {
        isRun = true;
    }
    public void freeView()
    {
        isRun = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isRun)
        {
            locked();
        }
        else
        {
            follow();
        }
    }
}
