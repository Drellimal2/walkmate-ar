using UnityEngine;
using System;


#if UNITY_WSA && !UNITY_EDITOR && ENABLE_WINMD_SUPPORT
using System.IO;
using Windows.Storage;
#endif

public class FileWriter : MonoBehaviour
{

    string fileName = "";
    private void Start()
    {
        float ctm = Time.time;
        fileName = ctm + ".txt";
        // Example: Call multiple functions that write to the same file
        WriteToFile("Function 1");
        WriteToFile("Function 2");
    }

    public void WriteToFile(string data)
    {
#if UNITY_WSA && !UNITY_EDITOR && ENABLE_WINMD_SUPPORT
        // Call the synchronous WriteToFile method only in UWP build
        WriteToFileSync(data);
#else
        // Handle non-UWP or Unity Editor case
        Debug.Log("WriteToFile is not supported in this environment.");
#endif
    }

#if UNITY_WSA && !UNITY_EDITOR && ENABLE_WINMD_SUPPORT
    private void WriteToFileSync(string data)
    {
        // Specify the file name
        // string fileName = "SampleFile.txt";

        // Get the app's local folder path
        string localFolderPath = ApplicationData.Current.LocalFolder.Path;

        // Combine the folder path and file name to get the full file path
        string filePath = Path.Combine(localFolderPath, fileName);

        try
        {
            // Write text to the file synchronously
            File.AppendAllText(filePath, data + "\n");

            // Optionally, read the contents back
            string readText = File.ReadAllText(filePath);

            // Output the read text to the console (for testing purposes)
            Debug.Log("Read from file: " + readText);
        }
        catch (Exception ex)
        {
            // Handle exceptions, e.g., insufficient permissions, file locks, etc.
            Debug.LogError("Error writing to file: " + ex.Message);
        }
    }
#endif
}
