﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class WalkMate : MonoBehaviour
{

    private InputController inputManager;

    const int DELAYSTEP = 2;

    public int steps_to_change = 15;

    float myu, k_lr_diff, k;

    public float delta_theta_d, delta_theta_ml, delta_theta_mr;

    float  omega_right, omega_left;
    float delta_left, delta_right;

    float theta_right, theta_left, theta_diff;
    float omega_MAX = 9f;
    float omega_MIN = 3f;
    //Human Phase
    float omega_human, theta_humanl, theta_humanr;
    //Foot Contact Flag
    bool left_foot, right_foot, isRun, startMove;
    bool left_sound, right_sound;
    //Time Counter
    float general_Cnt, step_Cnt;
    int step_Num;

    //Sound Play
    public GameObject WalkMateManager;
    AudioSource[] audios;

    public DataManager datastore;

    // Start is called before the first frame update
    void Start()
    {
        audios = WalkMateManager.GetComponents<AudioSource>();
        inputManager = WalkMateManager.GetComponent<InputController>();
        Init();
    }

    public void Init()
    {
        // myu = 0.15f;
        //k_lr_diff = 3f;
        k = 0.3f;
                // myu = 0.32f;

        myu = 0.42f;
        k_lr_diff = 3f;
        // k = 0.5f;

        // omega_right = 6f;
        // omega_left = 6f;
        omega_right = 4f;
        omega_left = 4f;
        theta_right = Mathf.PI;
        theta_left = 0;
        delta_theta_d = 0;
        delta_theta_ml = 0;

        omega_human = 6f;
        theta_humanl = 0;
        theta_humanr = Mathf.PI;

        left_foot = false;
        right_foot = false;
        isRun = false;
        startMove = false;
        left_sound = false;
        right_sound = false;

        general_Cnt = 0;
        step_Cnt = 0;
        step_Num = 0;
    }


    public void turnOnWalkMate()
    {
        isRun = true;
        step_Num = 0;
    }
    public void turnOffWalkMate()
    {
        Init();
    }
    public int getStepNum()
    {
        return step_Num;
    }

    // Update is called once per frame
    void Update()
    {
        if (isRun)
        {
            general_Cnt += Time.deltaTime;

            if (step_Num < DELAYSTEP || !startMove)
            {
                HumanOgemaEstimation();
                omega_right = omega_human;
                omega_left = omega_human;
            }
            else
            {
                HumanPhaseEstimation();
                HumanOgemaEstimation();
                RobotPhaseEstimation();
                RobotOgemaEstimation();
            }
        }
        string verboselog = "w::"+ delta_theta_d + "::" + theta_humanl + "::" + theta_humanr  + "::" + theta_left + "::" + theta_right + "::" + omega_left + "::" + omega_right + "::" + omega_human + "::" + delta_left + "::" + delta_right + "::" + delta_theta_ml + "::" + delta_theta_mr;
        datastore.LogData(verboselog);
    }

    void setHumanPhase2(float phase){
        theta_humanl = phase;
        theta_humanr = phase + Mathf.PI;
    }
    

    void HumanPhaseEstimation()
    {
        // theta_humanl += omega_human * Time.deltaTime;
        // theta_humanr += omega_human * Time.deltaTime;
        if (theta_humanl > 2 * Mathf.PI)
        {
            theta_humanl -= 2 * Mathf.PI;
        }
        if (theta_humanr > 2 * Mathf.PI)
        {
            theta_humanr -= 2 * Mathf.PI;
        }
    }
    void HumanOgemaEstimation()
    {
        float previous_Omega = omega_human;
        float present_Omega;
        step_Cnt += Time.deltaTime;
        if (left_foot || right_foot)
        {
            present_Omega = Mathf.PI / step_Cnt;
            
            if (Mathf.Abs(present_Omega - previous_Omega) > 3)
            {
                omega_human = previous_Omega;
            }
            else
            {
                omega_human = 0.1f * previous_Omega + 0.9f * present_Omega;//How about using PID 
            }

            omega_human = Mathf.Min(omega_human, omega_MAX);
            omega_human = Mathf.Max(omega_human, omega_MIN);
            

            step_Cnt = 0;
            if (step_Num >= DELAYSTEP && !startMove)
            {
                if (left_foot)
                {
                    theta_left = -delta_theta_d;
                    theta_right = Mathf.PI - delta_theta_d;
                    startMove = true;
                }
            }
            step_Num++;
            if (step_Num % steps_to_change == 0)
                inputManager.switchAnim();
            // if (left_foot)
            // {
            //     theta_humanl = omega_human * Time.deltaTime * 3;
            //     theta_humanr = Mathf.PI + omega_human * Time.deltaTime * 3;
            // }
            // if (right_foot)
            // {
            //     theta_humanl = Mathf.PI + omega_human * Time.deltaTime * 3;
            //     theta_humanr = omega_human * Time.deltaTime * 3;
            // }
            left_foot = false;
            right_foot = false;
        }
    }

    void RobotPhaseEstimation()
    {
        theta_diff = theta_right - theta_left;

        theta_diff = Mathf.Max(theta_diff, -Mathf.PI);
        theta_diff = Mathf.Min(theta_diff, Mathf.PI);

        
        delta_theta_ml = theta_humanl - theta_left;
        delta_theta_mr = theta_humanr - theta_right;

        delta_left = (omega_left  + k * Mathf.Sin(delta_theta_ml)) * Time.deltaTime;
        theta_left += delta_left;
        if (!left_sound & (theta_left > 2 * Mathf.PI - 8 * delta_left))
        {
            audios[0].Play();
            left_sound = true;
        }
        if (theta_left > 2 * Mathf.PI)
        {
            theta_left -= 2 * Mathf.PI;
            left_sound = false;
        }
        delta_right = (omega_right + k * Mathf.Sin(delta_theta_mr)) * Time.deltaTime;
        theta_right += delta_right;
        if (!right_sound & (theta_right > 2 * Mathf.PI - 8 * delta_right))
        {
            audios[1].Play();
            right_sound = true;
                   

        }
        if (theta_right > 2 * Mathf.PI)
        {
            theta_right -= 2 * Mathf.PI;
            right_sound = false;
        }
        datastore.LogData("as::" + left_sound + "::" + right_sound);        

    }
    void RobotOgemaEstimation()
    {
        omega_left += myu * Mathf.Sin(delta_theta_ml - delta_theta_d) * Time.deltaTime;
        if (omega_left > omega_MAX)
        {
            omega_left = omega_MAX;
        }
        if (omega_left < omega_MIN)
        {
            omega_left = omega_MIN;
        }
        omega_right += myu * Mathf.Sin(delta_theta_mr - delta_theta_d) * Time.deltaTime;
        if (omega_right > omega_MAX)
        {
            omega_right = omega_MAX;
        }
        if (omega_right < omega_MIN)
        {
            omega_right = omega_MIN;
        }
    }

    public float getwalkTime()
    {
        return general_Cnt;
    }
    public float getThetaDiff()
    {
        if (delta_theta_ml > Mathf.PI)
            delta_theta_ml -= 2 * Mathf.PI;
        if (delta_theta_ml < -Mathf.PI)
            delta_theta_ml += 2 * Mathf.PI;
        return delta_theta_ml;
    }
    public void setLeftFootOn()
    {
        left_foot = true;
        right_foot = false;
    }
    public void setRightFootOn()
    {
        right_foot = true;
        left_foot = false;
    }
    public void setTargetPhase(float src)
    {
        delta_theta_d = src;
    }

    public void setHumanPhase(float src)
    {
        theta_humanl = src;
        theta_humanr = src + Mathf.PI;
    }

    public float getTargetPhase()
    {
        return delta_theta_d;
    }
    public float getThetaLeft()
    {
        return theta_left;
    }
    public float getHumanPhase()
    {
        return theta_humanl;
    }
    public float getRobotOmega()
    {
        return omega_left;
    }
    public float getHumanOmega()
    {
        return omega_human;
    }

    

}
