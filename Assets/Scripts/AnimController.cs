﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour
{

    public GameObject WalkmateController;
    private WalkMate walkMate;
    private Animator animator;
    public DataManager datastore;

    float targetTime, switchtime, updateTime;
    bool mixture = false;
    public int anim = 50;

    public float weight = 0;
    int index = 0;

    public int changeIndex = 0;
    public int stride_inc = 0;
    int prev_index = 0;

    public List<string> anims = new List<string>{ "Base Layer.HumanoidWalk"};
    List<int> phases =new List<int>{0,0,0,0};

    public bool strideSwitched = false;
    public int prevAnimIndex = 0;
    public int currentAnimIndex = 1;
    public int ChangeTime = 15;
    // Use this for initialization


    // Start is called before the first frame update
    void Start()
    {
        walkMate = WalkmateController.GetComponent<WalkMate>();
        animator = gameObject.GetComponent<Animator>();
        init();
    }

    
    public void switchAnim(){
        Debug.LogFormat("Anim switch");

        mixture = true;
        
        // walkMate.setTargetPhase(phases[index] * Mathf.Deg2Rad);
    }

    public void switchStride(){
        strideSwitched = true;
        mixture = true;

    }
    public void switchStride(int inc){
        Debug.Log("Weights::" + animator.GetLayerWeight(0) + ":" + animator.GetLayerWeight(1) + ":" + animator.GetLayerWeight(2));
            if (!strideSwitched){
                mixture = true;
                strideSwitched = true;
            }
            stride_inc = inc;

    }

    public void init()
    {
        mixture = false;
        switchtime = 0;
        animator.SetLayerWeight(0, 1);
        animator.SetLayerWeight(1, 0);
        animator.SetLayerWeight(2, 0);
        anim = 50;
        index = 0;
        updateTime = 0;
        stride_inc = 0;
        prev_index = 0;
        strideSwitched = false;


    }

    public float getWeight()
    {
        return Math.Min(switchtime / ChangeTime, 1.0f);
        // return weight;
        // return 1.0f;
    }


    public void setAnims(List<int> vals){
        string fmt = "000.##";
        List<string> temp = new List<string>{};

        foreach(int Val in vals){
            temp.Add("walking" + Val.ToString(fmt));
        }
        anims = temp;
        foreach(string Val in anims){
            Debug.Log(Val);
        }
    }

    public void setPhases(List<int> vals){
        phases = vals;
    }

    public void updateStride(float val) {
        weight = val;

    }
    // Update is called once per frame
    void Update()
    {
        double thetaLeft = walkMate.getThetaLeft();
        double tl = thetaLeft;
        if (changeIndex == -1){
            // thetaLeft += Math.PI;
            Debug.Log("Ok");

        }
        tl += Math.PI;
        float antitargettime = (float)tl / Mathf.PI / 2;

        targetTime = (float)thetaLeft / Mathf.PI / 2;
        // animator.Play("Base Layer.HumanoidWalk", 0, targetTime);
        // Debug.Log("Animator" + thetaLeft);
        updateTime += Time.deltaTime;

        if (updateTime >= 0){
            updateTime = 0;

       

            if (!mixture)

            { 
                // if (changeIndex == -1){
                //     animator.Play(getAnim(changeIndex), getAnimLayer(changeIndex), antitargettime);//targetTime is in percentage

                // } else {
                animator.Play(getAnim(changeIndex), getAnimLayer(changeIndex), targetTime);//targetTime is in percentage
                animator.Play(getAnim(0), 0, targetTime);
                // }
                // animator.Play(anims[index], 0, targetTime);//targetTime is in percentage
            }
            else
            {
                if (switchtime < ChangeTime)
                    switchtime += Time.deltaTime;
                else
                    mixture = false;
                    animator.SetLayerWeight(getAnimLayer(changeIndex), getWeight());

                    animator.SetLayerWeight(0, 1 - getWeight());
                    // if (changeIndex == -1){
                    //     animator.Play(getAnim(changeIndex), getAnimLayer(changeIndex), antitargettime);//targetTime is in percentage

                    // } else {
                    animator.Play(getAnim(changeIndex), getAnimLayer(changeIndex), targetTime);//targetTime is in percentage

                    // }
                    animator.Play(getAnim(0), 0, targetTime);

            }
         }
         float www = getWeight();
         string verboselog = "ac::"+ targetTime + "::" + switchtime + "::" + getWeight() + "::" + mixture;
        
        datastore.LogData(verboselog);   
    }

    private String getAnim(int index){
        switch(index){
            case 1:
                return "momoage";
                // return "largestride";

            case -1:
                // return "walking000";
                return "smallstride";

            default:
                return "Base Layer.HumanoidWalk";
        }


    }

     private int getAnimLayer(int index){
        switch(index){
            case 1:
                return 1;
            case -1:
                return 2;
            default:
                return 0;
        }


    }


    public void momoage()
    {

        if(!mixture)
        {
            mixture = true;
        }
    }

    public void momoage(int ind)
    {
        changeIndex = ind;
        if (ind != 0){
            mixture = true;
        }
        
    }

    public void momoage(int ind, int tTC)
    {
        ChangeTime = tTC;
        changeIndex = ind;
        if (ind != 0){
            mixture = true;
        }
        
    }


    public void momoage(bool mix)
    {

        mixture = mix;
    }
}
