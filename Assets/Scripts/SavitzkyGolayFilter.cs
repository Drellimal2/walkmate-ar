﻿using System;
using UnityEngine;

public class SavitzkyGolayFilter : MonoBehaviour
{
    public int windowSize = 5;
    public int polynomialOrder = 3;

    public float output;
    private float[] coefficients;

    private void Start()
    {
        coefficients = CalculateCoefficients(windowSize, polynomialOrder);
    }

    private void Update()
    {
        float[] data = new float[windowSize];
        for (int i = 0; i < windowSize; i++)
        {
            data[i] = Camera.main.transform.position.y;
            ; // Replace with your input data
        }

        output = Convolve(coefficients, data);

        // Do something with the filtered output, for example:
        Debug.Log(output);
    }

    private float[] CalculateCoefficients(int windowSize, int polynomialOrder)
    {
        if (windowSize % 2 == 0 || polynomialOrder > windowSize)
        {
            throw new ArgumentException("Invalid window size or polynomial order");
        }

        int m = (windowSize - 1) / 2;
        float[] x = new float[windowSize];
        float[] y = new float[windowSize];
        float[] b = new float[polynomialOrder + 1];
        float[,] a = new float[polynomialOrder + 1, polynomialOrder + 1];

        for (int i = -m; i <= m; i++)
        {
            for (int j = 0; j <= polynomialOrder; j++)
            {
                x[i + m] = i;
                a[j, 0] += Mathf.Pow(i, j);
                y[i + m] = 1.0f;
            }
        }

        for (int j = 1; j <= polynomialOrder; j++)
        {
            for (int k = 0; k <= polynomialOrder; k++)
            {
                for (int i = -m; i <= m; i++)
                {
                    a[k, j] += Mathf.Pow(i, k + j);
                }
            }
        }

        for (int j = 0; j <= polynomialOrder; j++)
        {
            for (int i = -m; i <= m; i++)
            {
                b[j] += Mathf.Pow(i, j) * y[i + m];
            }
        }

        float[,] aa = new float[polynomialOrder + 1, polynomialOrder + 1];
        for (int i = 0; i <= polynomialOrder; i++)
        {
            for (int j = 0; j <= polynomialOrder; j++)
            {
                aa[i, j] = a[i, j];
            }
        }

        GaussianElimination(aa, b, polynomialOrder + 1);

        float[] c = new float[windowSize];
        for (int i = -m; i <= m; i++)
        {
            for (int j = 0; j <= polynomialOrder; j++)
            {
                c[i + m] += b[j] * Mathf.Pow(i, j);
            }
        }

       
    return c;
}

private float Convolve(float[] coefficients, float[] data)
{
    if (coefficients.Length != data.Length)
    {
        throw new ArgumentException("Length of coefficients and data must be equal");
    }

    float sum = 0.0f;
    for (int i = 0; i < coefficients.Length; i++)
    {
        sum += coefficients[i] * data[i];
    }

    return sum;
}

private void GaussianElimination(float[,] a, float[] b, int n)
{
    for (int k = 0; k < n; k++)
    {
        int i_max = k;
        float max = a[k, k];

        for (int i = k + 1; i < n; i++)
        {
            if (Mathf.Abs(a[i, k]) > Mathf.Abs(max))
            {
                i_max = i;
                max = a[i, k];
            }
        }

        for (int j = k; j < n; j++)
        {
            float temp = a[k, j];
            a[k, j] = a[i_max, j];
            a[i_max, j] = temp;
        }

        float temp2 = b[k];
        b[k] = b[i_max];
        b[i_max] = temp2;

        for (int i = k + 1; i < n; i++)
        {
            float factor = a[i, k] / a[k, k];
            b[i] -= factor * b[k];
            for (int j = k; j < n; j++)
            {
                a[i, j] -= factor * a[k, j];
            }
        }
    }

    for (int i = n - 1; i >= 0; i--)
    {
        float sum = 0.0f;
        for (int j = i + 1; j < n; j++)
        {
            sum += a[i, j] * b[j];
        }

        b[i] = (b[i] - sum) / a[i, i];
    }
}
}