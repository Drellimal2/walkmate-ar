﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour
{
    public float timeLeft = 30.0f;
    
    public TMP_Text countdownText;

    void Update()
    {
        if(timeLeft > 0){
            timeLeft -= Time.deltaTime;
        }
        countdownText.text = "Please stay still for\n" + string.Format("{0:0}", timeLeft) + "s";
 
        if (timeLeft <= 0)
        {
            countdownText.gameObject.SetActive(false);
            // Perform action here (restart game, display game over message, etc.)
        }
    }
}
