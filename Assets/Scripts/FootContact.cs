﻿using System;
using System.Linq;

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;


public class FootContact : MonoBehaviour {
    public SavitzkyGolayFilter MainFilter;
    public GameObject WalkMateManager;
    private InputController inputController;
    private WalkMate walkmate;

    public DataManager datastore;
    public TextMeshProUGUI textM;
    
    const int SIZE = 6, AVERAGE_WIDTH = SIZE - 1;
    float[] frontz, frontx;
    const int NUMSIZE = 300;

    List<double> maxs, mins;
    bool calibrated = false;
    int ccount = 0;
    double[] y_array;
    double[] thetas;
    double vert_avg, vert_avg2;
    double prevtheta, currenttheta;
    double[] ydot, ddot;
    double[] vertical, mediolateral;
    double[] filteredVert;
    double testxz;

    double filteredY;
    double[] front, frontdw;
    double currentHeight, previousHeight;

    double currentFilteredHeight, previousFilteredHeight;
    double currentDirection, previousDirection;
    double xdis = 0;
    double maxY, minY, maxVert, minVert;
    double zpos, xpos;
    bool isRun = false, contact;
    bool left, right;
    bool stance = true;
    bool isLeft = false;
    string state;
    double thetamain = 0;
    double currentTheta = 0;
    double prevTheta = 0;


        // public int windowSize = 5;
    
    public int windowSize = 9;

    public int polynomialOrder = 5;

    private float[] coefficients;
    
    // Start is called before the first frame update
    void Start()
    {
        walkmate = WalkMateManager.GetComponent<WalkMate>();
        MainFilter = WalkMateManager.GetComponent<SavitzkyGolayFilter>();
        inputController = WalkMateManager.GetComponent<InputController>();
        datastore.LogData("=======================================\nSTART NEW\n=======================================");
        Init();
        filteredY = Camera.main.transform.position.y;
        coefficients = CalculateCoefficients(windowSize, polynomialOrder);
         for (int i = 0; i < NUMSIZE; i++)
        {
            y_array[i] = filteredY;
        }

    }

    void Init()
    {
        vertical = new double[SIZE];
        filteredVert = new double[SIZE];

        mediolateral = new double[SIZE];


        ydot = new double[NUMSIZE];
        ddot = new double[SIZE];
        front = new double[SIZE];
        frontdw = new double[SIZE];
        thetas =  new double[NUMSIZE];
        vert_avg = 0;
        vert_avg2 =  Camera.main.transform.position.y;
        frontx = new float[SIZE];
        y_array = new double[NUMSIZE];
        frontz = new float[SIZE];

        zpos = Camera.main.transform.position.z;
        xpos = Camera.main.transform.position.x;
        maxs = new List<double>();
        mins = new List<double>();

        // textM.text = "Test";

        contact = false;
        maxY = -1000f;
        maxVert = -1000f;
        minY = 1000f;
        minVert = 1000f;
        isRun = false;
        state = "Unknown";
    }

    // Update is called once per frame
    void Update()
    {
        String path = datastore.logurl;
        if (datastore.log){
            Debug.Log(path);
        }
        for (int i = 1; i < SIZE; i++)
        {
            frontz[SIZE - i] = frontz[SIZE - i - 1];
            frontx[SIZE - i] = frontx[SIZE - i - 1];
            front[SIZE - i] = front[SIZE - i - 1];
        }
        frontz[0] = Camera.main.transform.position.z;
        frontx[0] = Camera.main.transform.position.x;
        front[0] = Math.Cos(Camera.main.transform.eulerAngles.z) * frontz[0] + Math.Sin(Camera.main.transform.eulerAngles.z) * frontx[0];
        
        testxz = Math.Sqrt ((Math.Abs(frontz[0] - frontz[SIZE - 1]) * Math.Abs(frontz[0] - frontz[SIZE - 1])) + 
                (Math.Abs(frontx[0] - frontx[SIZE - 1]) * Math.Abs(frontx[0] - frontx[SIZE - 1]))) ;
                frontdw[0] = frontz[0] * frontx[0] / front[0];
        frontdw[0] = frontz[0] * frontx[0] / testxz;
        xdis += frontdw[0];
        


        if (testxz > 0.05)
            inputController.start();
        else if (testxz < 0.001){
            inputController.shutdown();
            Debug.Log("shutdown");
        }
        
        // if (Math.Abs(frontz[0] - frontz[SIZE - 1]) > 0.05)
        //     inputController.start();
        // else if (Math.Abs(frontz[0] - frontz[SIZE - 1]) < 0.001){
        //     inputController.shutdown();
        // }

        // if (Math.Abs(front[0] - front[SIZE - 1]) > 0.05)
        //     inputController.start();
        // else if (Math.Abs(front[0] - front[SIZE - 1]) < 0.001){
        // else if (Math.Abs(front[0] - front[SIZE - 1]) < 0.03){
        //     inputController.shutdown();
        // }
            // inputController.shutdown();
        
        
        if (isRun)
        {
            footContactDetect();
            leftRightDetect();
            if(contact)
            {
                walkmate.setRightFootOn();
                if (Camera.main.transform.position.z - zpos < 0)
                {
                    if (right)
                    {
                        walkmate.setLeftFootOn();
                        // walkmate.setHumanPhase((float)theta);

                        state = "Left";
                    }
                    if (left)
                    {
                        // walkmate.setHumanPhase((float)theta + Math.PI);
                        walkmate.setRightFootOn();
                        state = "Right";
                    }
                }else
                {
                    if (right)
                    {

                        walkmate.setRightFootOn();
                        state = "Right";
                    }
                    if (left)
                    {
                        
                        walkmate.setLeftFootOn();
                        state = "Left";
                    }
                }
                // textM.text = state;
                contact = false;
            }
        }
        zpos = Camera.main.transform.position.z;
   
    }

    void footContactDetect()
    {
        float[] data = new float[windowSize];
        float posY = Camera.main.transform.position.y;

       
        for (int i = 0; i < windowSize; i++)
        {
            data[i] = posY;
        }

        filteredY = Convolve(coefficients, data);

        // Array.Copy(vertical, 1, vertical, 0, SIZE - 1);
        // Array.Copy(filteredVert, 1, filteredVert, 0, SIZE - 1);
        // Array.Copy(y_array, 1, y_array, 0, NUMSIZE - 1);
        // Array.Copy(ydot, 1, ydot, 0, NUMSIZE - 1);
        // Array.Copy(thetas, 1, thetas, 0, NUMSIZE - 1);
         for (int i = 1; i < SIZE; i++)
        {
            vertical[SIZE - i] = vertical[SIZE - i - 1];
            filteredVert[SIZE - i] = filteredVert[SIZE - i - 1];
        }
         for (int j = 1; j < NUMSIZE; j++)
        {
            y_array[NUMSIZE - j] = y_array[NUMSIZE - j - 1];
            ydot[NUMSIZE - j] = ydot[NUMSIZE - j - 1];
            thetas[NUMSIZE - j] = thetas[NUMSIZE - j - 1];
        }

        y_array[0] = filteredY;

        // datastore.LogData("rd::" + vert_avg + "::" + String.Join("::", y_array));        

        vertical[0] = posY;
        filteredVert[0] = filteredY;

        double currentAverage = 0;
        double previousAverage = 0;
        double currentFiltAverage = 0;
        double previousFiltAverage = 0;

        for (int i = 0; i < AVERAGE_WIDTH; i++)
        {
            currentAverage += vertical[i];
            previousAverage += vertical[i + 1];
            currentFiltAverage += filteredVert[i];
            previousFiltAverage += filteredVert[i + 1];
        }
        currentAverage /= AVERAGE_WIDTH;
        previousAverage /= AVERAGE_WIDTH;
        currentFiltAverage /= AVERAGE_WIDTH;
        previousFiltAverage /= AVERAGE_WIDTH;
        
       
        if (currentAverage > 0.007)
        {
            maxY = (double) Mathf.Max((float)maxY, (float)currentAverage); 
            minY = minY >= currentAverage ? currentAverage : minY;
        }

        // if (currentFiltAverage > 0.0007)
        // {
            maxVert = maxVert <= filteredY ? filteredY : maxVert;
            minVert = minVert >= filteredY ? filteredY : minVert;
        // }
        
        currentHeight = currentAverage - previousAverage;
        currentFilteredHeight = currentFiltAverage - previousFiltAverage;
        ydot[0] = currentFilteredHeight/Time.deltaTime;
        double ydotmean = ydot.Sum() / NUMSIZE;
        vert_avg = (mins.Sum() + maxs.Sum())/(mins.Count + maxs.Count);
        
        if (previousFilteredHeight <= 0 && currentFilteredHeight > 0 && maxVert - minVert > 0.005f)
        {

            if(stance){

                stance = false;
                contact = true;
                mins.Add(filteredY);
                if (mins.Count == 1){
                    mins.Add(filteredY);
                }
                if(maxs.Count == 0){
                    maxs.Add(maxVert);
                    maxs.Add(maxVert);
                }
                maxVert = maxs[maxs.Count - 2];
                if(contact){
                    calibrated = true;
                    if (Camera.main.transform.position.z - zpos < 0)
                    {
                        if(left){
                            thetamain = Math.PI;

                        } else {
                            thetamain = 2 * Math.PI;

                        }
                    } else {
                        if(right){
                            thetamain = Math.PI;

                        } else {
                            thetamain = 2 * Math.PI;

                        }
                    }
                }
            } else {
                maxs[maxs.Count - 1] = maxVert;
            }
            
        }
        bool ccf = false;
        if (previousFilteredHeight > 0 && currentFilteredHeight <= 0 && maxVert - minVert > 0.005f && !
        stance)
        {
            stance = true;
            maxs.Add(filteredY);

            if (maxs.Count == 1){
                maxs.Add(filteredY);
            }  
            if(mins.Count == 0){
                mins.Add(minVert);
                mins.Add(minVert);
            }
            ccf = true;
            minVert = mins[mins.Count - 2]; //1000

            // maxY = maxs[maxs.Count - 2];
            
            
        }
        double f_y =  Math.Max(minVert, Math.Min(maxVert, filteredY));
        double stand_filt_y  = -1 + ((f_y - minVert)*2/(maxVert - minVert));
        double stand_filt_ydot = -1 + ((ydot[0] - ydot.Min())*2/(ydot.Max() - ydot.Min()));
        currentTheta = -Math.Atan2(stand_filt_ydot, stand_filt_y); // Maybe better using just y
        double theta_diff_curr_prev = currentTheta - prevTheta;
        if(theta_diff_curr_prev < 0){
            theta_diff_curr_prev = 0;
        }
        if (!calibrated) {
            thetamain += (theta_diff_curr_prev/2);
            
            thetamain %= (2*Math.PI);
        }
        // if (Camera.main.transform.position.z - zpos < 0){
        //         thetamain += Math.PI;
        //         // walkmate.setHumanPhase(tt);
        //     }
        // else {
        walkmate.setHumanPhase((float)thetamain);

        // }
        
        thetas[0] = thetamain;

        int fc = contact ? 1 : 0;
        string verboselog = "v::"+ vertical[0] + "::" + maxVert + "::" + minVert + "::" + currentFiltAverage + "::" + previousFiltAverage  + "::" + currentHeight + "::" + stand_filt_y + "::" + filteredY  + "::" + currentFilteredHeight  + "::" + Time.deltaTime  + "::" + fc  + "::" + ccf + "::" + thetamain+ "::" + currentTheta  + "::" + stand_filt_ydot   + "::" +  vert_avg;
        
        datastore.LogData(verboselog);        

        previousHeight = currentHeight;
        previousFilteredHeight = currentFilteredHeight;
        calibrated = false;
        prevTheta = currentTheta;
    } 
    void leftRightDetect()
    {
        for (int i = 1; i < SIZE; i++)
        {
            mediolateral[SIZE - i] = mediolateral[SIZE - i - 1];
            ddot[SIZE - i] = ddot[SIZE - i - 1];

        }
        mediolateral[0] = Camera.main.transform.position.x;

        double currentAverage = 0;
        double previousAverage = 0;
        for (int i = 0; i < AVERAGE_WIDTH; i++)
        {
            currentAverage += mediolateral[i];
            previousAverage += mediolateral[i + 1];
        }
        currentAverage /= AVERAGE_WIDTH;
        previousAverage /= AVERAGE_WIDTH;

        currentDirection = currentAverage - previousAverage;
        ddot[0] = frontdw[0]/Time.deltaTime;
        double theta = Math.Atan2(ddot[0], xdis);

        string url = "http://192.168.100.170:5000/log/d::" + theta + "::" +  ddot[0] + "::" + xdis;
        //left local maximum -> right foot contact
        if (previousDirection < 0 && currentDirection >= 0 && !right)
        {
            left = false;
            isLeft = false;
            right = true;
        }
        //right local maximum -> left foot contact
        if (previousDirection > 0 && currentDirection <= 0 && !left)
        {
            right = false;
            left = true;
            isLeft = true;
        }
        previousDirection = currentDirection;
        string verboselog = "h::"+ Camera.main.transform.position.x + "::" + Camera.main.transform.position.z + "::" + currentAverage + "::" + previousAverage  + "::" + currentDirection + "::" + previousDirection + "::" + left + "::" + right;
        datastore.LogData(verboselog);        
    } 

    public void turnOnDetect()
    {
        isRun = true;
    }
    public void turnOffDetect()
    {
        // Init();
    }
    public string getFootState()
    {
        return state;
    }



    public static double GetMedian(double[] sourceNumbers) {
        //Framework 2.0 version of this method. there is an easier way in F4        
        if (sourceNumbers == null || sourceNumbers.Length == 0)
            throw new System.Exception("Median of empty array not defined.");

        //make sure the list is sorted, but use a new array
        double[] sortedPNumbers = (double[])sourceNumbers.Clone();
        Array.Sort(sortedPNumbers);

        //get the median
        int size = sortedPNumbers.Length;
        int mid = size / 2;
        double median = (size % 2 != 0) ? (double)sortedPNumbers[mid] : ((double)sortedPNumbers[mid] + (double)sortedPNumbers[mid - 1]) / 2;
        return median;
    }


    private float Convolve(float[] coefficients, float[] data)
    {
        if (coefficients.Length != data.Length)
        {
            throw new ArgumentException("Length of coefficients and data must be equal");
        }

        float sum = 0.0f;
        for (int i = 0; i < coefficients.Length; i++)
        {
            sum += coefficients[i] * data[i];
        }

        return sum;
    }

    private void GaussianElimination(float[,] a, float[] b, int n)
    {
        for (int k = 0; k < n; k++)
        {
            int i_max = k;
            float max = a[k, k];

            for (int i = k + 1; i < n; i++)
            {
                if (Mathf.Abs(a[i, k]) > Mathf.Abs(max))
                {
                    i_max = i;
                    max = a[i, k];
                }
            }

            for (int j = k; j < n; j++)
            {
                float temp = a[k, j];
                a[k, j] = a[i_max, j];
                a[i_max, j] = temp;
            }

            float temp2 = b[k];
            b[k] = b[i_max];
            b[i_max] = temp2;

            for (int i = k + 1; i < n; i++)
            {
                float factor = a[i, k] / a[k, k];
                b[i] -= factor * b[k];
                for (int j = k; j < n; j++)
                {
                    a[i, j] -= factor * a[k, j];
                }
            }
        }

        for (int i = n - 1; i >= 0; i--)
        {
            float sum = 0.0f;
            for (int j = i + 1; j < n; j++)
            {
                sum += a[i, j] * b[j];
            }

            b[i] = (b[i] - sum) / a[i, i];
        }
    }

    private float[] CalculateCoefficients(int windowSize, int polynomialOrder)
    {
        if (windowSize % 2 == 0 || polynomialOrder > windowSize)
        {
            throw new ArgumentException("Invalid window size or polynomial order");
        }

        int m = (windowSize - 1) / 2;
        float[] x = new float[windowSize];
        float[] y = new float[windowSize];
        float[] b = new float[polynomialOrder + 1];
        float[,] a = new float[polynomialOrder + 1, polynomialOrder + 1];

        for (int i = -m; i <= m; i++)
        {
            for (int j = 0; j <= polynomialOrder; j++)
            {
                x[i + m] = i;
                a[j, 0] += Mathf.Pow(i, j);
                y[i + m] = 1.0f;
            }
        }

        for (int j = 1; j <= polynomialOrder; j++)
        {
            for (int k = 0; k <= polynomialOrder; k++)
            {
                for (int i = -m; i <= m; i++)
                {
                    a[k, j] += Mathf.Pow(i, k + j);
                }
            }
        }

        for (int j = 0; j <= polynomialOrder; j++)
        {
            for (int i = -m; i <= m; i++)
            {
                b[j] += Mathf.Pow(i, j) * y[i + m];
            }
        }

        float[,] aa = new float[polynomialOrder + 1, polynomialOrder + 1];
        for (int i = 0; i <= polynomialOrder; i++)
        {
            for (int j = 0; j <= polynomialOrder; j++)
            {
                aa[i, j] = a[i, j];
            }
        }

        GaussianElimination(aa, b, polynomialOrder + 1);

        float[] c = new float[windowSize];
        for (int i = -m; i <= m; i++)
        {
            for (int j = 0; j <= polynomialOrder; j++)
            {
                c[i + m] += b[j] * Mathf.Pow(i, j);
            }
        }

       
    return c;
}



    public  double GetStandard(double[] sourceNumbers) {
        //Framework 2.0 version of this method. there is an easier way in F4        
        if (sourceNumbers == null || sourceNumbers.Length == 0)
            throw new System.Exception("Median of empty array not defined.");

        //make sure the list is sorted, but use a new array
        double[] sortedPNumbers = (double[])sourceNumbers.Clone();
        Array.Sort(sortedPNumbers);

        //get the median
        int size = sortedPNumbers.Length;
        int mid = size / 2;
        double median = (size % 2 != 0) ? (double)sortedPNumbers[mid] : ((double)sortedPNumbers[mid] + (double)sortedPNumbers[mid - 1]) / 2;
        return median;
    }
}